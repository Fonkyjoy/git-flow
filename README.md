# Git flow #  

## Nom des branches: ##
- **Feature** => évolution du projet.
- **Bugfix**/**Hotfix** => correction de bug _(hotfix part toujours de master)_.
- **Release** => ajout d'une version _(le merge s'effectue sur le develop et sur master)_.


## Commandes: ##
**git flow init**  => initialise le projet, une branche développe se crée en automatique.

**git flow feature start** _nom_de_la_branche_ => Création d'une nouvelle branche.

**git flow feature finish** _nom_de_la_branche_ => Commit la branche et la merge en local.   
delete la branche sur le distant.  
!! Penser à faire un push de la branche concernée sur le distant. !!

**git flow release start** _nom_de_la_version_ => Creation d'une feature en relation avec le main.  
**git flow release finish** _nom_de_la_version_ => merge la feature sur le branche en local  
!! Penser à faire un push sur le distant. !!  

# Git #  
**git reset nom_du_tag --hard** => revenir sur une version prcédéente a partir d'un numéro de tag  
!! attention cette commande modifie le gitlog !!  


